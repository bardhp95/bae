import math
from abc import ABC, abstractmethod

__author__ = 'Hamed Sarvari, Bardh Prenkaj, Giovanni Stilo'

""" 
    This class represents the decay ratio from one
    layer to the next in the DeepSymAE.
"""
class DecayAB(ABC):
 
    def __init__(self, val):
        self.val = val
        super().__init__()
    
    @abstractmethod
    def shrink(self, l, n_d):
        return self.val * l * n_d

class LinearDecay(DecayAB):
    
    def shrink(self, l, n_d):
        return math.floor(self.val * n_d)

class ExpDecay(DecayAB):

    def shrink(self, l, n_d):
        return math.ceil(n_d * math.exp(-(self.val * l)))

class ConstantDecay(DecayAB):

    def shrink(self, l, n_d):
        return self.val[l]

class CompositeDecay():
    def __init__(self, shrinkers, choices, size):
        self.shrinkers = shrinkers
        self.choices = choices + [0]
        self.size = size

    def shrink(self, l, n_d):
        for i in range(len(self.choices)):
            if n_d/self.size >= self.choices[i]:
                return self.shrinkers[i].shrink(l, n_d)
