import numpy as np
import pandas as pd
import torch
from scipy.io.arff import loadarff
from scipy.io.matlab import loadmat
from sklearn import preprocessing

__author__ = 'Hamed Sarvari, Bardh Prenkaj, Giovanni Stilo'


class DataLoader:

    def __init__(self, filename, normalise=True,\
        b=-1.0, f=1.0):

        self.filename = filename
        self.normalise = normalise
        self.b = b
        self.f = f
       

    def __data_init(self):
        try:
            data = None

            if self.filename.endswith('.arff'):
                # load the arff file
                data = loadarff(self.filename)

                # transform the arff data into a pandas dataframe
                data = pd.DataFrame(data[0])

            elif self.filename.endswith('.csv'):
                data = pd.read_csv(self.filename)

            elif self.filename.endswith('.mat'):
                data = loadmat(self.filename)

                data = pd.DataFrame(np.hstack((data['X'],\
                    data['y'])))

            # if the column 'id' is in the column list of the data,
            # then remove it
            if 'id' in data.columns:
                data = data.drop(labels='id', axis=1)

            dummies = pd.get_dummies(data[data.columns[-1]], drop_first=True)
            data = data.drop(data.columns[-1], axis = 1)
            data = data.join(dummies)

            data = data.rename(columns={\
                data.columns[-1]: 'outlier'})

            if self.normalise == True:
                x = data.loc[:, data.columns != 'outlier'] 
                # scale column-wise data in [self.b, self.f]
                # add the outlier column to the scaled data
                data = self.__scale_data(x,\
                        self.b, self.f).join(data['outlier'])
            
            return data
        except NotImplementedError:
            print('The file %s contains not supported attributes.'\
                %self.filename)

    def __scale_data(self, X, b, f, min=0, max=255, opt=True):
        if opt == True:
            max = X.max(axis=0)
            min = X.min(axis=0)

        diff = max - min

        X = (f-b) * (X-min)/diff + b

        # case in which the minimium and maximum of a column
        # are the same
        X.dropna(axis='columns')
        X = X.fillna((f+b)/2)

        return X


    def prepare_data(self):
        data = self.__data_init()
        # get the ground truth labels
        y = data['outlier'].values

        if 'id' in data.columns:
            X = data.drop(labels='id', axis=1)
        else:
            X = data
        X = X.drop(labels='outlier', axis=1)

        # normalise the data to [0,1]
        X = torch.tensor(X.values, dtype=torch.float32)

        print("Input tensor shape {0}".format(X.shape))
        print("Ground truth tensor shape {0}".format(y.shape))


        return X, y

