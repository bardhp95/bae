# HEADER FILE #
from consensus.policy import combine_weighted
from torch.nn import MSELoss
from layer_decays.decay import LinearDecay

######################################################
__author__ = 'Hamed Sarvari, Bardh Prenkaj, Giovanni Stilo'
######################################################
# when constructing an AE, it indicates whether
# you should use an odd number of layers or not
# BOTTLENECK == True -> odd 
# BOTTLENECK == False -> even
BOTTLENECK = True
# The number of training epochs for each 
# iteration
EPOCHS = 50
# indicates the maximum of encoder layers to train
# the model
MAX_ENC_LAYERS = 4
# The number of folds for which we repeat the
# experiments
FOLDS = 30
# The maximum number of iterations
ITERATIONS = 20
# The loss optimisation function to use
loss = MSELoss()
# Consensus policy to adopt when combining the 
# reconstruction errors of each data point
consensus = combine_weighted
# shrinker that calculates the neurons
# of the layers in the autoencoder
shrinker = LinearDecay(0.5)
# stop training threshold
STOP_TH = 1e-4
######################################################
# Common Optimiser Parameters
# Learning rate
LR = 1e-3
WEIGHT_DECAY = 1e-5
######################################################
# Flag that determines the data normalisation
NORM = True
# The value at the beginning of the normalisation
# interval
B_NORM = 0.0
# The value at the end of the normalisation interval
E_NORM = 1.0
# Data Path
DPATH = '../resources/data/'
# The list of datasets to read and train on
DATASETS = ['Lympho.csv']
######################################################
