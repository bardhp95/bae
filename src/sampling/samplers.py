import torch 
import numpy as np

__author__ = 'Hamed Sarvari, Bardh Prenkaj, Giovanni Stilo'

"""
    Class that performs the weighted sampling
    based on the errors generated by the ensemble
    components of BAE
"""
class WeightedSampler:

    
    def __init__(self, X):
        """
        Parameters:
            - X (torch.tensor): The original 
                    input tensor
        """
        self.X = X

    """
        This function takes a subset of the entire
        tensor X and returns that subsample. The
        subsample is selected with replacement.

        Parameters:
            - errors (numpy.array): The rec errors 
                    of iteration it
            - it (int): The iteration of the training
                    phase
        return torch.tensor representing the subsample
            of the original data
    """
    def sample(self, errors, it):
        # select all the instances in the first
        # iterations
        if it == 0:
            return self.X
        else:
            # calculate the weights as described in
            # the paper
            weights = (1 / errors)/np.sum(1 / errors)
            # sample the indices with replacement
            indices = np.random.choice(list(range(\
                len(errors))), self.X.shape[0], p=weights)
            # return the subsample
            return self.X[indices]