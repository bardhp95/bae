import torch
import torch.nn as nn
import copy
import numpy as np

__author__ = 'Hamed Sarvari, Bardh Prenkaj, Giovanni Stilo'

class FullyConnectedAE(nn.Module):

    def __init__(self, enc_layers, dim,\
        shrinker, bottleneck=True, _min=3,
        device='cpu'):

        super(FullyConnectedAE, self).__init__()

        self.enc_layers = enc_layers
        self.dim = dim
        self.min = _min
        self.bottleneck = bottleneck
        self.shrinker = shrinker
        self.device = device

        self.build()

    def build(self):
        layers = []
        sizes = [self.dim]
        
        #Encoder Layers
        for l in range(self.enc_layers):
            n_h = max([self.shrinker.shrink(l+1,\
                sizes[-1]), self.min])

            layers.append(nn.Linear(sizes[-1], n_h).to(self.device) )
            # add a sigmoid layer as the first 
            # activation function
            if l == 0:
                layers.append(nn.Sigmoid().to(self.device))
            else: # the other activation layers are ReLU
                layers.append(nn.ReLU(inplace=False).to(self.device))

            sizes.append(n_h)

            if n_h == 1 or self.enc_layers == -1:
                break

        #Decoder Layers
        for l in range(self.enc_layers, 1, -1):
            layers.append(nn.Linear(sizes[l], sizes[l-1]).to(self.device))
            layers.append(nn.ReLU(inplace=False).to(self.device))
        
        layers.append(nn.Linear(sizes[1], sizes[0]).to(self.device))
        layers.append(nn.Sigmoid().to(self.device))


        # if this flag is set then we need an
        # odd number of layers
        # the last layers is going to be placed at
        # the centre of the model. Hence, the
        # bottleneck naming. Because an
        # autoencoder is symmetric, the bottleneck's
        # input and output size are equal to the last
        # encoder layer's output size.
        if self.bottleneck == True:
            # copy the encoder layers
            temp = copy.deepcopy(layers[0:self.enc_layers*2])
            # add the bottleneck
            temp.append(nn.Linear(sizes[-1], sizes[-1]).to(self.device))
            temp.append(nn.ReLU(inplace=False).to(self.device))
            # add the old layers of the decoder
            temp += layers[self.enc_layers*2:]
            layers = copy.deepcopy(temp)

        self.net = nn.Sequential(*layers)

        print(self.net)

    def forward(self, x):
        return self.net(x)

    """
        Function that resets the parameter weights of 
        the layers in a neural network. The layers
        in our case are all fully connected (torch.nn.Linear)

        Params:
            m (torch.nn.Module): the layer to reset weights
    """
    def weight_reset(self, m):
        if isinstance(m, torch.nn.Linear):
            m.reset_parameters()